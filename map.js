let map;

function initMap() {
    const PARIS = { lat: 48.85661400000001, lng: 2.3522219000000177 };
    map = new google.maps.Map(document.getElementById('map'), {
    center: PARIS,
    zoom: 12,
  });
}

function initPano(pos) {
  // Note: constructed panorama objects have visible: true
  // set by default.
  let Marker = new google.maps.Marker({
    position: pos,
    map,
  });

  Marker.addListener('rightclick', function () {
    const panorama = new google.maps.StreetViewPanorama(
      document.getElementById('map'),
      {
        position: pos,
        addressControlOptions: {
          position: google.maps.ControlPosition.BOTTOM_CENTER,
        },
        linksControl: true,
        panControl: true,
        enableCloseButton: true,
        pov: {
          heading: 34,
          pitch: 10
        }
      }
    );
  })
}
