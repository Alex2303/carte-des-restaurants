fetch("http://localhost/ApiMap/data.json")
.then((response) => response.json())
.then(function(data) {
    data.forEach((element) => {
        const coords = { lat: element.lat, lng: element.long };
        initPano(coords);
    });
   
    formatData(data); //create a new array using map function to display elements
    
})

.catch(function(error) {
    console.log('Il y a eu un problème avec l\'opération fetch: ' + error.message);
});

function formatData(data) {

    document.getElementById('list-group').innerHTML = data
    .map(({restaurantName,address, ratings}) => 
    `<li class="list-group-item bold">${restaurantName}</li> 
     <li class="list-group-item">${address}</li>
     <button class="btn-custom showComments">Voir avis</button>
     <li class="list-group-item ratings hidden">
         ${ratings
          .map(({stars,comment}) => 
            `${comment}<br>
            ${addIcon("<i class=\"fas fa-star\"></i>", stars)} ${restIcon("<i class=\"far fa-star\"></i>", stars)} <br>
           ` //to include a variable or a statement in the template literal, you wrap it in ${....}
            
          ).join("")}
     </li>`
   ).join("");
   showComments();
}

function addIcon(str, n) {
    return str.repeat(n);
}

function restIcon(str, n) {
    return str.repeat(5 - n);
}

function showComments() {
    let btnShow = document.querySelectorAll('.showComments'),
    ratings = document.querySelectorAll('.ratings');
    
    btnShow.forEach((btn, i) => {
        btn.addEventListener('click', function() {
            ratings.item(i).classList.toggle('show');
            console.log(ratings.item(i));
        })
    });
}





